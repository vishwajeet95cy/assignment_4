var UserModel = require('./models/userModel');
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/mydb', { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true }, (err, db) => {
  if (err) throw err;
  console.log('Database Connect');
})

UserModel.deleteMany({}, function () {
  console.log('Database Cleared');
});

var usermodel = [
  new UserModel({
    name: 'Vishwajeet Kumar',
    age: 20,
    email: 'vkskpkk@gmail.com',
    gender: 'Male',
    role: 'Affiliate'
  }),
  new UserModel({
    name: 'Vikash Kumar',
    age: 22,
    gender: 'Male',
    email: 'vikash25@gmail.com',
  }),
  new UserModel({
    name: 'Saloni Kumari',
    age: 22,
    gender: 'Female',
    email: 'saloni22@gmail.com',
    role: 'Mobile'
  })
];

var done = 0;
for (var i = 0; i < usermodel.length; i++) {
  usermodel[i].save(function (err, result) {
    if (err) {
      console.log(err);
      return;
    };

    done++;
    if (done === usermodel.length) {
      exist()
    };
  });
};

function exist() {
  mongoose.disconnect()
}