const mongoose = require('mongoose');

let getId = () => {
  return '_' + Math.random().toString(36).substr(2, 9);
}

const userSchema = new mongoose.Schema({
  role: {
    type: String,
    default: 'Admin',
    enum: ['Admin', 'Mobile', 'Affiliate']
  },
  apiKey: {
    type: String,
    default: getId()
  },
  date: {
    type: Date,
    default: new Date().toLocaleString()
  },
  name: {
    type: String,
  },
  age: {
    type: String
  },
  gender: {
    type: String
  },
  email: {
    type: String,
    unique: true
  }
}, {
  timestamps: Date
})

module.exports = mongoose.model('UserModel', userSchema)