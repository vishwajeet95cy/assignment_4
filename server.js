const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose')
const dotenv = require('dotenv');
const morgan = require('morgan')
dotenv.config();
const sampleRoute = require('./routes/sampleRoute')

const app = express();

mongoose.connect(process.env.DB_HOST, { useNewUrlParser: true, useUnifiedTopology: true }, (err, db) => {
  if (err) throw err;
  console.log('Database Connected');
});

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors())
app.use(morgan('dev'))

app.use('/user', sampleRoute)

app.get('/', (req, res) => {
  res.send("Welcome to express")
})

app.listen(process.env.HOST, () => {
  console.log(`Server is listing at: http://localhost:${process.env.HOST}`)
})