const router = require('express').Router()
const nodemailer = require('nodemailer');


router.post('/sendMail', function (req, res, next) {
  const name = req.body.name;
  const email = req.body.email;
  const number = req.body.number
  const message = req.body.message;

  const data = `<ul><li>Name: ${name}</li><li>Number: ${number}</li><li>Email: ${email}</li><li>Message: ${message}</li></ul>`
  async function main() {
    // create reusable transporter object using the default SMTP transport
    var transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: process.env.email,
        pass: process.env.pass
      }
    });

    // send mail with defined transport object
    let info = await transporter.sendMail({
      from: '"React App 👻" <vkskpkk@gmail.com>', // sender address
      to: email, // list of receivers
      subject: "Contact", // Subject line
      html: data, // html body
    });

    if (info.messageId) {
      res.send("Email Sent")
    } else {
      res.send("Error with Sending Mail");
    }

    console.log("Message sent: %s", info.messageId);
  }

  main().catch(console.error)
})



module.exports = router