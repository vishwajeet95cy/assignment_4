module.exports = {
  async up(db, client) {
    // TODO write your migration here.
    // See https://github.com/seppevs/migrate-mongo/#creating-a-new-migration-script
    // Example:
    // await db.collection('albums').updateOne({artist: 'The Beatles'}, {$set: {blacklisted: true}});
    await db.collection('roles').insert([
      {
        name: "vishwa",
        age: 25,
        email: "vishwajeet95cy@gmail.com",
        gender: "Male"
      },
      {
        name: "Shiva",
        age: 25,
        email: "shiva25@gmail.com",
        gender: "Male"
      },
      {
        name: "saloni",
        age: 25,
        email: "saloni@gmail.com",
        gender: "Male"
      }
    ])
  },

  async down(db, client) {
    // TODO write the statements to rollback your migration (if possible)
    // Example:
    // await db.collection('albums').updateOne({artist: 'The Beatles'}, {$set: {blacklisted: false}});
    await db.collection('roles').deleteMany({})
  }
};
